<p align="center"><a href="https://laravel.com" target="_blank"><img width="230" src="https://laravel.com/assets/img/components/logo-laravel.svg" alt="Logo Laravel"></a></p>
<br>

> **Remember!!** Untuk DEMO langsung WEB ini bisa langsung ke **dparagon.arif.app**.
<h1>SISTEM TEST KERJA</h1>
<br>

<br>
<h2>Cara install project ini :</h2>
<ul>
  <li><p>1. Install GIT <code><a href="https://git-scm.com/" target="_blank">https://git-scm.com/</a></code></p></li>
  <li><p>2. Install Composer <code><a href="https://getcomposer.org/" target="_blank">https://getcomposer.org/</a></code></p></li>
  <li><p>4. Ketikan di Git Bash/CMD/Terminal/PowerShell <code>git clone https://gitlab.com/arifpujinugroho/testdparagon.git</code></p></li>
  <li><p>5. Masuk ke folder project-nya atau ketikan <code>cd testdparagon</code></p></li>
  <li><p>6. Ketikan<code>composer update</code></p></li>
  <li><p>9. Duplikat .env.example dan Ubah nama file duplikat tersebut menjadi .env atau ketikan <code>cp .env.example .env</code></p></li>
  <li><p>10. Ketikan<code>php artisan key:generate</code></p></li>
  <li><p>11. Masukan semua nama database, username database, dan password database yang ada didalam file <code>.env</code>.</p></li>
  <li><p>12. Migrate database setelah diatur, atau ketikan <code>php artisan migrate</code> jika sudah ada table dalam databse maka <code>php artisan migrate:fresh</code>.</p></li>
  <li><p>13. Running menggunakan <code>php artisan serve</code> atau bisa langsung DEMO ke <code><a href="https://dparagon.arif.app" target="_blank">https://dparagon.arif.app</a></code></li>
  <li><p>14. <strong>Jangan Lupa untuk syukuri apapun</strong> yang terjadi pada WEB ini 😊</p></li>
</ul>

<h2>Credits</h2>
<ul>
    <li><a href="https://github.com/laravel/laravel" target="_blank">Laravel Framework Web</a></li>
    <li><a href="https://github.com/stisla/stisla" target="_blank">Stisla Bootstrap 4 Admin Panel Template</a></li>
    <li><a href="https://momentjs.com/" target="_blank">Moment.js</a></li>
</ul>

