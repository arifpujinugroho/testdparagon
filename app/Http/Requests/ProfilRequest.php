<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|sometimes|max:191',
            'password'  => 'required|sometimes|confirmed|string|min:8',
            'foto'      => 'required|sometimes|image|file|max:1024',
            'telepon'   => 'required|sometimes|numeric|digits_between:10,14|unique:users,telepon,'.auth()->user()->id,
        ];
    }
}
