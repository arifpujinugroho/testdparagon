<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfilRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response view
     */
    public function index()
    {
        return view('auth.profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ProfilRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfilRequest $request)
    {
        $data = $request->all();

        if($request->hasFile('foto')){
            $data['foto'] = $request->file('foto')->store(
                'assets/profil','public'
            );
        }

        if($request->password){
            $data['password'] = Hash::make($request->password);
        }

        User::findOrFail(auth()->user()->id)->update($data);
    }
}
