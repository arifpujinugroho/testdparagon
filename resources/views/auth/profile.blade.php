@extends('layouts.app')
@section('title')Update Profile @endsection
@section('header-body')Update Profile @endsection
@section('breadcrumb')<div class="breadcrumb-item">Profile</div>@endsection


@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <input type="hidden" id="queryURL">
                    <h4>Data Profile</h4>
                    <div class="card-header-action">
                        <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalProfil"> Edit Profil</button>
                        <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalPassword"> Ganti Password</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 text-center">
                            @if(auth()->user()->foto == null)
                            <img src="{{auth()->user()->foto}}" class="img-fluid" alt="Foto Profil">
                            @else
                            <img src="{{asset('assets/img/avatar/avatar-1.png')}}" class="img-fluid" alt="Foto Profil">
                            @endif
                            <br><button class="btn btn-sm btn-warning btn-block mt-3" data-toggle="modal" data-target="#modalFoto"> Ganti Profil</button>
                        </div>
                        <div class="col-6">
                            Nama : {{auth()->user()->name}} <br>
                            Telepon : {{auth()->user()->telepon}} <br>
                            Email : {{auth()->user()->email}} <br>
                            Alamat : {{auth()->user()->alamat}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('end')
<!-- Modal -->
<div class="modal fade" id="modalProfil" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Profil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form id="formProfil" method="POST"> @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="namabarang">Nama <strong class="text-danger">*</strong></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="name" id="name" value="{{auth()->user()->name}}" placeholder="Nama Lengkap (Harus Disi)" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="namabarang">Telepon <strong class="text-danger">*</strong></label>
                <div class="input-group">
                    <input type="number" class="form-control" name="telepon" id="telepon" value="{{auth()->user()->telepon}}" placeholder="Nomer Telepon (Harus Disi)" required>
                </div>
                </div>
                <div class="form-group">
                    <label for="namabarang">Alamat (Optional)</label>
                <div class="form-group">
                  <textarea class="form-control" name="alamat" id="alamat" rows="3" placeholder="Alamat (Optional)">{{auth()->user()->alamat}}</textarea>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="submitProfil btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form id="formPassword" method="POST"> @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="namabarang">Password <strong class="text-danger">*</strong></label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password (Harus Disi)" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="namabarang">Confirm Password <strong class="text-danger">*</strong></label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Ulangi Password (Harus Disi)" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="submitPassword btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalFoto" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ganti Foto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form id="formFoto" method="POST"> @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="namabarang">Foto Profil <strong class="text-danger">*</strong></label>
                    <div class="input-group">
                        <input type="file" class="form-control" name="foto" id="foto" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="submitFoto btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection


@push('js')
<script src="{{ asset('assets/js/jquery.form.min.js')}}"></script>
@include('auth.js')
@endpush
