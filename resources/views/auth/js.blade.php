<script>
var urlnya = "{{route('profil.update')}}";

$(".submitProfil").click(function(){
    $('.notifError').remove();
    $(':input').removeClass('is-invalid');

    $('#formProfil').ajaxForm({
        url: urlnya,
        beforeSend: function() {
            $('#btnAction').addClass('btn-progress disabled');
            $('#btnAction').attr('disabled','disabled');
        },
    	error: function (response) {
            $('#btnAction').removeAttr('disabled');
            $('#btnAction').removeClass('btn-progress disabled');
            $.each(response.responseJSON.errors,function(field_name,error){
                $(document).find('[name='+field_name+']').parent().after('<span class="notifError text-strong text-danger"> <strong>' +error+ '</strong></span>');
                $(document).find('[name='+field_name+']').addClass('is-invalid');
            });
    	},
        success: function(xhr) {
            $('.progress').hide();
            $('#btnAction').removeAttr('disabled');
            $('#btnAction').removeClass('btn-progress disabled');
            $(':input').removeClass('is-invalid');
            location.reload();
        }
    });
});
$(".submitPassword").click(function(){
    $('.notifError').remove();
    $(':input').removeClass('is-invalid');

    $('#formPassword').ajaxForm({
        url: urlnya,
        beforeSend: function() {
            $('#btnAction').addClass('btn-progress disabled');
            $('#btnAction').attr('disabled','disabled');
        },
    	error: function (response) {
            $('#btnAction').removeAttr('disabled');
            $('#btnAction').removeClass('btn-progress disabled');
            $.each(response.responseJSON.errors,function(field_name,error){
                $(document).find('[name='+field_name+']').parent().after('<span class="notifError text-strong text-danger"> <strong>' +error+ '</strong></span>');
                $(document).find('[name='+field_name+']').addClass('is-invalid');
            });
    	},
        success: function(xhr) {
            $('.progress').hide();
            $('#btnAction').removeAttr('disabled');
            $('#btnAction').removeClass('btn-progress disabled');
            $(':input').removeClass('is-invalid');
            location.reload();
        }
    });
});
$(".submitFoto").click(function(){
    $('.notifError').remove();
    $(':input').removeClass('is-invalid');

    $('#formFoto').ajaxForm({
        url: urlnya,
        beforeSend: function() {
            $('#btnAction').addClass('btn-progress disabled');
            $('#btnAction').attr('disabled','disabled');
        },
    	error: function (response) {
            $('#btnAction').removeAttr('disabled');
            $('#btnAction').removeClass('btn-progress disabled');
            $.each(response.responseJSON.errors,function(field_name,error){
                $(document).find('[name='+field_name+']').parent().after('<span class="notifError text-strong text-danger"> <strong>' +error+ '</strong></span>');
                $(document).find('[name='+field_name+']').addClass('is-invalid');
            });
    	},
        success: function(xhr) {
            $('.progress').hide();
            $('#btnAction').removeAttr('disabled');
            $('#btnAction').removeClass('btn-progress disabled');
            $(':input').removeClass('is-invalid');
            location.reload();
        }
    });
});
</script>
