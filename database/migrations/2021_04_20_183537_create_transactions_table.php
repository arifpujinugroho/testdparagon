<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('kode');
            $table->string('name');
            $table->string('email');
            $table->string('number');
            $table->enum('gender',['L','P']);
            $table->string('address');
            $table->integer('transaction_total');
            $table->string('transaction_status');
            $table->enum('metode',['Cash','GOPay','OVO','BRI','QRIS','ShopeePay'])->default('Cash');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
